生成DEF文件

dumpbin /exports zmotion_x64.dll> zmotion_x64.def

生成的文件如下:

Microsoft (R) COFF/PE Dumper Version 14.00.24210.0
Copyright (C) Microsoft Corporation.  All rights reserved.


Dump of file zmotion_x64.dll

File Type: DLL

  Section contains the following exports for zmotion.dll

    00000000 characteristics
    54101CA1 time date stamp Wed Sep 10 17:40:49 2014
        0.00 version
           1 ordinal base
         136 number of functions
         136 number of names

    ordinal hint RVA      name

        134    0 0000CA20 DllCanUnloadNow
        135    1 0000C9C0 DllGetClassObject
        136    2 0000CA60 DllRegisterServer
          1    3 00003FB0 ZMC_BreakAdd
          2    4 00004160 ZMC_BreakClear
          3    5 00004090 ZMC_BreakDel
          4    6 00001EB0 ZMC_CalStringHash
          5    7 000039C0 ZMC_CancelOnline
          6    8 00001F80 ZMC_Check3File
          7    9 00002C50 ZMC_CheckFirmWare
          8    A 000034E0 ZMC_CheckProgramSyntax
          9    B 00003190 ZMC_ClearLog
         10    C 00001450 ZMC_Close
         11    D 000020B0 ZMC_Delete3File
         12    E 00003300 ZMC_DeleteFile
         13    F 000016F0 ZMC_DirectCommand
         14   10 00003260 ZMC_DownFile
         15   11 00002DA0 ZMC_DownFirmWare
         16   12 00003280 ZMC_DownMemFile
         17   13 000018A0 ZMC_DownMemZar
         18   14 000019E0 ZMC_DownMemZarToRamAndRun
         19   15 000017A0 ZMC_DownZar
         20   16 000018D0 ZMC_DownZarToRamAndRun
         21   17 00003E80 ZMC_EnumArray
         22   18 00003C80 ZMC_EnumChildCard
         23   19 00003E80 ZMC_EnumLabel
         24   1A 00003D60 ZMC_EnumLocal
         25   1B 00003E80 ZMC_EnumPara
         26   1C 00003E80 ZMC_EnumSub
         27   1D 00003E80 ZMC_EnumVar
         28   1E 00004530 ZMC_Execute
         29   1F 00001650 ZMC_ExecuteGetReceive
         30   20 000016C0 ZMC_ExecuteGetRemainBuffSpace
         31   21 00004430 ZMC_ExecuteNoAck
         32   22 00001610 ZMC_ExecuteWaitDown
         33   23 00004210 ZMC_FastOpen
         34   24 00002020 ZMC_FindFirst3File
         35   25 00003200 ZMC_FindFirstFile
         36   26 00002050 ZMC_FindNext3File
         37   27 00003230 ZMC_FindNextFile
         38   28 00002160 ZMC_FlashReadf
         39   29 000020F0 ZMC_FlashWritef
         40   2A 000026F0 ZMC_GetAIn
         41   2B 000034F0 ZMC_GetAxisFeatures
         42   2C 000025C0 ZMC_GetAxisStates
         43   2D 000015E0 ZMC_GetAxises
         44   2E 00002270 ZMC_GetChipId
         45   2F 00002230 ZMC_GetClientId
         46   30 00001570 ZMC_GetConnectString
         47   31 00001540 ZMC_GetConnectType
         48   32 00002F40 ZMC_GetControllerIDDate
         49   33 00002080 ZMC_GetCur3File
         50   34 00002200 ZMC_GetCurIpAddr
         51   35 000027A0 ZMC_GetDaOut
         52   36 000034B0 ZMC_GetErrcodeDescription
         53   37 00002250 ZMC_GetError
         54   38 00003BD0 ZMC_GetFileState
         55   39 00002260 ZMC_GetHardId
         56   3A 00002480 ZMC_GetIn
         57   3B 000029A0 ZMC_GetInAll
         58   3C 000031D0 ZMC_GetMac
         59   3D 000034E0 ZMC_GetMaxPciCards
         60   3E 00002A50 ZMC_GetOutAll
         61   3F 00002520 ZMC_GetOutput
         62   40 00001510 ZMC_GetProgress
         63   41 00002240 ZMC_GetSoftId
         64   42 00002280 ZMC_GetSoftVersion
         65   43 000015A0 ZMC_GetState
         66   44 000015B0 ZMC_GetStopPauseTaskid
         67   45 000037D0 ZMC_GetStringType
         68   46 00003510 ZMC_GetSysSpecification
         69   47 00003AE0 ZMC_GetTaskStackState
         70   48 000039D0 ZMC_GetTaskState
         71   49 000014D0 ZMC_GetTimeOut
         72   4A 00001EA0 ZMC_GetZlibGlobalDefine
         73   4B 00003F20 ZMC_GlobalArrayGet
         74   4C 00003E90 ZMC_GlobalVarGet
         75   4D 00002BC0 ZMC_JumpApp
         76   4E 00002BD0 ZMC_JumpZbios
         77   4F 00004720 ZMC_Lock
         78   50 00003120 ZMC_LowFormatNand
         79   51 00001C90 ZMC_MakeOneFileZpj
         80   52 00001E10 ZMC_MakeRealZpj
         81   53 00001E80 ZMC_MakeRealZpjMem
         83   54 00001760 ZMC_MakeZar
         82   55 00001780 ZMC_MakeZar2
         85   56 00001BB0 ZMC_MakeZarAndDown
         84   57 00001C20 ZMC_MakeZarAndDown2
         87   58 00001AD0 ZMC_MakeZarAndRamRun
         86   59 00001B40 ZMC_MakeZarAndRamRun2
         88   5A 00001E90 ZMC_MakeZlib
         89   5B 00003390 ZMC_Modbus_Get0x
         90   5C 000033F0 ZMC_Modbus_Get4x
         91   5D 00003330 ZMC_Modbus_Set0x
         92   5E 00003450 ZMC_Modbus_Set4x
         93   5F 00003E80 ZMC_ModifyLocal
         94   60 00004170 ZMC_Open
         95   61 000042C0 ZMC_OpenCom
         97   62 00004390 ZMC_OpenEth
         96   63 000043A0 ZMC_OpenEth2
         98   64 00001100 ZMC_OpenPci
         99   65 00001F60 ZMC_Pause
        100   66 00004320 ZMC_PeakCom
        101   67 000036E0 ZMC_ReadMessage
        102   68 000020E0 ZMC_RemoveAll3Files
        103   69 00003140 ZMC_ResetController
        104   6A 00001F50 ZMC_Resume
        105   6B 00003130 ZMC_ReturnDefault
        106   6C 00001F00 ZMC_RunZarFile
        107   6D 000043C0 ZMC_SearchAndOpenCom
        108   6E 00001110 ZMC_SearchAndOpenEth
        109   6F 00001340 ZMC_SearchEth
        110   70 00002340 ZMC_SetAxisEnable
        111   71 000010E0 ZMC_SetComDefaultBaud
        112   72 00003040 ZMC_SetControllerIDDate
        113   73 00002850 ZMC_SetDaOut
        114   74 000031A0 ZMC_SetMac
        115   75 000028F0 ZMC_SetOutAll
        116   76 000023E0 ZMC_SetOutput
        117   77 00001490 ZMC_SetTimeOut
        118   78 00004920 ZMC_Signal_BasicP
        119   79 00004940 ZMC_Signal_BasicV
        120   7A 00002BB0 ZMC_StartHardwareTest
        121   7B 00003900 ZMC_StepRun
        122   7C 00001F70 ZMC_Stop
        123   7D 00002BE0 ZMC_TempModBaud
        124   7E 00002B00 ZMC_TestSoftware
        125   7F 000047C0 ZMC_UnLock
        126   80 00001ED0 ZMC_UpCurZpjToMem
        127   81 000032B0 ZMC_UpFile
        128   82 000032D0 ZMC_UpFileToMem
        129   83 00003150 ZMC_UpLog
        130   84 00003160 ZMC_UpLogToMem
        131   85 00003E80 ZMC_UpZar
        132   86 00003E80 ZMC_UpZarToMem
        133   87 00003E80 ZMC_UpZarToProject

  Summary

       18000 .data
       14000 .pdata
       83000 .rdata
       13000 .reloc
        3000 .rsrc
      176000 .text
        1000 data
        1000 text

修改def文件为标准def格式

将zmotion_x64.def修改保存如下文件格式：

LIBRARY

EXPORTS
    ZMC_BreakAdd
    ZMC_BreakClear
    ZMC_BreakDel
    ZMC_CalStringHash
    ZMC_CancelOnline
    ZMC_Check3File
    ZMC_CheckFirmWare
    ZMC_CheckProgramSyntax
    ZMC_ClearLog
    ZMC_Close
    ZMC_Delete3File
    ZMC_DeleteFile
    ZMC_DirectCommand
    ZMC_DownFile
    ZMC_DownFirmWare
    ZMC_DownMemFile
    ZMC_DownMemZar
    ZMC_DownMemZarToRamAndRun
    ZMC_DownZar
    ZMC_DownZarToRamAndRun
    ZMC_EnumArray
    ZMC_EnumChildCard
    ZMC_EnumLabel
    ZMC_EnumLocal
    ZMC_EnumPara
    ZMC_EnumSub
    ZMC_EnumVar
    ZMC_Execute
    ZMC_ExecuteGetReceive
    ZMC_ExecuteGetRemainBuffSpace
    ZMC_ExecuteNoAck
    ZMC_ExecuteWaitDown
    ZMC_FastOpen
    ZMC_FindFirst3File
    ZMC_FindFirstFile
    ZMC_FindNext3File
    ZMC_FindNextFile
    ZMC_FlashReadf
    ZMC_FlashWritef
    ZMC_GetAIn
    ZMC_GetAxisFeatures
    ZMC_GetAxisStates
    ZMC_GetAxises
    ZMC_GetChipId
    ZMC_GetClientId
    ZMC_GetConnectString
    ZMC_GetConnectType
    ZMC_GetControllerIDDate
    ZMC_GetCur3File
    ZMC_GetCurIpAddr
    ZMC_GetDaOut
    ZMC_GetErrcodeDescription
    ZMC_GetError
    ZMC_GetFileState
    ZMC_GetHardId
    ZMC_GetIn
    ZMC_GetInAll
    ZMC_GetMac
    ZMC_GetMaxPciCards
    ZMC_GetOutAll
    ZMC_GetOutput
    ZMC_GetProgress
    ZMC_GetSoftId
    ZMC_GetSoftVersion
    ZMC_GetState
    ZMC_GetStopPauseTaskid
    ZMC_GetStringType
    ZMC_GetSysSpecification
    ZMC_GetTaskStackState
    ZMC_GetTaskState
    ZMC_GetTimeOut
    ZMC_GetZlibGlobalDefine
    ZMC_GlobalArrayGet
    ZMC_GlobalVarGet
    ZMC_JumpApp
    ZMC_JumpZbios
    ZMC_Lock
    ZMC_LowFormatNand
    ZMC_MakeOneFileZpj
    ZMC_MakeRealZpj
    ZMC_MakeRealZpjMem
    ZMC_MakeZar
    ZMC_MakeZar2
    ZMC_MakeZarAndDown
    ZMC_MakeZarAndDown2
    ZMC_MakeZarAndRamRun
    ZMC_MakeZarAndRamRun2
    ZMC_MakeZlib
    ZMC_Modbus_Get0x
    ZMC_Modbus_Get4x
    ZMC_Modbus_Set0x
    ZMC_Modbus_Set4x
    ZMC_ModifyLocal
    ZMC_Open
    ZMC_OpenCom
    ZMC_OpenEth
    ZMC_OpenEth2
    ZMC_OpenPci
    ZMC_Pause
    ZMC_PeakCom
    ZMC_ReadMessage
    ZMC_RemoveAll3Files
    ZMC_ResetController
    ZMC_Resume
    ZMC_ReturnDefault
    ZMC_RunZarFile
    ZMC_SearchAndOpenCom
    ZMC_SearchAndOpenEth
    ZMC_SearchEth
    ZMC_SetAxisEnable
    ZMC_SetComDefaultBaud
    ZMC_SetControllerIDDate
    ZMC_SetDaOut
    ZMC_SetMac
    ZMC_SetOutAll
    ZMC_SetOutput
    ZMC_SetTimeOut
    ZMC_Signal_BasicP
    ZMC_Signal_BasicV
    ZMC_StartHardwareTest
    ZMC_StepRun
    ZMC_Stop
    ZMC_TempModBaud
    ZMC_TestSoftware
    ZMC_UnLock
    ZMC_UpCurZpjToMem
    ZMC_UpFile
    ZMC_UpFileToMem
    ZMC_UpLog
    ZMC_UpLogToMem
    ZMC_UpZar
    ZMC_UpZarToMem
    ZMC_UpZarToProject

生成LIB文件

lib /def:zmotion_x64.def /machine:x64 /out:zmotion_x64.lib